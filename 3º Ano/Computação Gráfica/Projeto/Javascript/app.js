

document.addEventListener('DOMContentLoaded', Start);

var cena = new THREE.Scene();

//Inicializar a camaras
//A camara 1 é a cmara em primeira pessoa
//A camara 2 é a camara ortogonal
//A camara 3 é a camara em terceira pessoa
var camara =  new THREE.PerspectiveCamera(75, window.innerWidth/window.innerHeight,0.1,1000);
var camara2 = new THREE.OrthographicCamera(-1.73,1.73,1,-1,0.10,2000);
camara2.scale.x = 60;
camara2.scale.y = 70;
camara2.rotation.x = -Math.PI/2
camara2.position.y = 10;
var camara3 = new THREE.PerspectiveCamera(75, window.innerWidth/window.innerHeight,0.1,1000);

var renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth-15,window.innerHeight-15);
document.body.appendChild(renderer.domElement);


//Textura do chao-----------------------------------------
const textureFloor = new THREE.TextureLoader().load('./Objetos/chao_1.png');
const planeGeo = new THREE.PlaneBufferGeometry(150,150);
const planeMet = new THREE.MeshPhongMaterial( {
    map: textureFloor,
    side: THREE.DoubleSide,
});
const floor = new THREE.Mesh(planeGeo,planeMet);
floor.rotation.x=-Math.PI/2;
floor.position.y = -8
cena.add(floor);


//Textura das paredes e do ceu-----------------------------------------------
const textureWall = new THREE.TextureLoader().load('./Objetos/parede.jpg');
const skyWall = new THREE.TextureLoader().load('./Objetos/ceu.png');

const wallGeo = new THREE.PlaneBufferGeometry(150,150);
const skyGeo = new THREE.PlaneBufferGeometry(200,200);

const wallMet = new THREE.MeshPhongMaterial( {
    map: textureWall,
    side: THREE.DoubleSide,
});
const SkyMet = new THREE.MeshPhongMaterial( {
    map: skyWall,
    side: THREE.DoubleSide
})


const sky = new THREE.Mesh(skyGeo, SkyMet);
cena.add(sky);
sky.rotation.x=-Math.PI/2;
sky.position.y = 100;

const wall_1 = new THREE.Mesh(wallGeo, wallMet);
wall_1.position.z = -75;
wall_1.position.y= 25;
cena.add(wall_1);

const wall_2 = new THREE.Mesh(wallGeo, wallMet);
wall_2.position.z = 75;
wall_2.position.y= 25;
cena.add(wall_2);

const wall_3 = new THREE.Mesh(wallGeo,wallMet);
wall_3.position.x = 75;
wall_3.position.y= 25;
wall_3.rotation.y = Math.PI/2
cena.add(wall_3);

const wall_4 = new THREE.Mesh(wallGeo,wallMet);
wall_4.position.x = -75;
wall_4.position.y= 25;
wall_4.rotation.y = -Math.PI/2
cena.add(wall_4);

//-------------------------


//Objeto importado-------------------------------
var objetoImportado;
var mixerAnimacao;
var relogio = new THREE.Clock();
var importer = new THREE.FBXLoader();

importer.load('./Objetos/run.fbx', function (object) {
  
    //Importar a animaçao diretamente do objeto
    mixerAnimacao = new THREE.AnimationMixer(object);
    var action = mixerAnimacao.clipAction(object.animations[0]);
    action.play();
    object.traverse(function (child){
        if (child.isMesh){
            child.castShadow = true;
            child.receiveShadow = false;
        }
    });

    //Adicionar o objeto a cena
    cena.add(object);

    //Escalar o objeto
    object.scale.x = 0.1;
    object.scale.z = 0.1;
    object.scale.y = 0.1;

    //Passar o objeto para uma variavel global
    objetoImportado = object;

    //colocar o objeto importado numa posição aleatoria
    objetoImportado.position.x= THREE.Math.randInt(-50,50);
    objetoImportado.position.z= THREE.Math.randInt(-50,50);

});


//No seguinte seguemento de codigo é importado 4 postes de luz para dar luz a cena
//Os 4 postes sao iguais mas apenas vao ser posicionados em lugares diferentes
//Como visto no objeto "inimigo" esta função é utilizada para importar objetos FBX
var lamp_1;
var lamp_2;
var lamp_3;
var lamp_4;

importer.load('./Objetos/lampada.fbx', function (object) {
    object.traverse(function (child){
        if (child.isMesh){
            child.castShadow = true;
            child.receiveShadow = false;
        }
    });
    //Adicionar o objeto a cena
    cena.add(object);

    //Escalar o objeto
    object.scale.x = 0.05;
    object.scale.z = 0.05;
    object.scale.y = 0.05;

    //Posicionar o objeto 
    object.position.y = -8;
    object.position.x = 60;
    object.position.z = 60;

    //Rodar a lampada
    object.rotation.y = Math.PI /1.32

    lamp_1 = object;
});

//------------------------------------------------------------------
importer.load('./Objetos/lampada.fbx', function (object) {
    object.traverse(function (child){
        if (child.isMesh){
            child.castShadow = true;
            child.receiveShadow = false;
        }
    });
    //Adicionar o objeto a cena
    cena.add(object);

    //Escalar o objeto
    object.scale.x = 0.05;
    object.scale.z = 0.05;
    object.scale.y = 0.05;

    //Posicionar o objeto 
    object.position.y = -8;
    object.position.x = -60;
    object.position.z = -60;

    //Rodar a lampada
    object.rotation.y = Math.PI/-4;

    lamp_2 = object;
});

//------------------------------------------------------------------
importer.load('./Objetos/lampada.fbx', function (object) {
    object.traverse(function (child){
        if (child.isMesh){
            child.castShadow = true;
            child.receiveShadow = false;
        }
    });
    //Adicionar o objeto a cena
    cena.add(object);

    //Escalar o objeto
    object.scale.x = 0.05;
    object.scale.z = 0.05;
    object.scale.y = 0.05;

    //Posicionar o objeto 
    object.position.y = -8;
    object.position.x = -60;
    object.position.z = 60;

    //Rodar a lampada
    object.rotation.y = Math.PI /4

    lamp_3 = object;
});

//--------------------------------------------------------
importer.load('./Objetos/lampada.fbx', function (object) {
    object.traverse(function (child){
        if (child.isMesh){
            child.castShadow = true;
            child.receiveShadow = false;
        }
    });
    //Adicionar o objeto a cena
    cena.add(object);

    //Escalar o objeto
    object.scale.x = 0.05;
    object.scale.z = 0.05;
    object.scale.y = 0.05;

    //Posicionar o objeto 
    object.position.y = -8;
    object.position.x = 60;
    object.position.z = -60;

    //Rodar a lampada
    object.rotation.y = Math.PI /-1.32;

    lamp_4 = object;
});

//Da mesma forma que temos os postes, precisamos das fontes de luz
var luz_1 = new THREE.SpotLight("lightyellow", 0.7);
luz_1.position.x = 70
luz_1.position.z = 70
luz_1.position = 2
luz_1.angle = 0.4
luz_1.penumbra = 1
cena.add(luz_1);

var luz_2 = new THREE.SpotLight("lightyellow", 0.7);
luz_2.position.x = -70
luz_2.position.z = -70
luz_2.position = 2
luz_2.angle = 0.4
luz_2.penumbra = 1
cena.add(luz_2);

var luz_3 = new THREE.SpotLight("lightyellow", 0.7);
luz_3.position.x = -70
luz_3.position.z = 70
luz_3.position = 2
luz_3.angle = 0.4
luz_3.penumbra = 1
cena.add(luz_3);

var luz_4 = new THREE.SpotLight("lightyellow", 0.7);
luz_4.position.x = 70
luz_4.position.z = -70
luz_4.position = 2
luz_4.angle = 0.4
luz_4.penumbra = 1
cena.add(luz_4);



//Textura da Bola que é disparada---------------------------------------------------
    var bola = new THREE.Group();
    var formato_bola = new THREE.SphereGeometry(0.3,0.3,0.3);

    var material_bola = new THREE.MeshBasicMaterial({color: 0x0eaeae});
    var _bola = new THREE.Mesh(formato_bola,material_bola);

    var overthebola = new THREE.CircleGeometry(1,32);
    var otb = new THREE.Mesh(overthebola,material_bola);
     

    //A variavel otb é um disco colocado por cima da bola para que esta seja visivel na camara ortogonal
    otb.position.y = 5;
    otb.rotation.x = -Math.PI/2;    
    bola.add(otb);
    bola.add(_bola);

//Criar objeto------------------------------------

//Criar formatos e groups
var personagem = new THREE.Group();
var cabeca_formato = new THREE.BoxGeometry(0.7,0.7,0.7);
var corpo_formato = new THREE.BoxGeometry(1,1.090,0.438);
var braco_formato = new THREE.BoxGeometry(0.300,1.270,0.300);
var perna_formato = new THREE.BoxGeometry(0.393,1.500,0.283);

//Estes objetos sao utilizados para a camara ortogonal
var overthehead =  new THREE.CircleGeometry(1.2,32);
var overthehead2 = new THREE.PlaneGeometry(1,2);

var material_overthehead = new THREE.MeshBasicMaterial({color: 0xa97d64});
var material_cabeca_braco = new THREE.MeshStandardMaterial({color: 0xa97d64});
var material_tronco = new THREE.MeshStandardMaterial({color: 0x0eaeae});
var material_perna = new THREE.MeshStandardMaterial({color: 0x494697});

var cabeca = new THREE.Mesh(cabeca_formato, material_cabeca_braco);
var corpo = new THREE.Mesh(corpo_formato, material_tronco);
var br1 = new THREE.Mesh(braco_formato, material_cabeca_braco);
var br2 = new THREE.Mesh(braco_formato, material_cabeca_braco);
var pr1 = new THREE.Mesh(perna_formato, material_perna);
var pr2 = new THREE.Mesh(perna_formato, material_perna);

var oth = new THREE.Mesh (overthehead, material_overthehead);
var oth2 = new THREE.Mesh(overthehead2, material_overthehead);


oth2.position.y = 7;
oth2.rotation.x = -Math.PI/2;
oth2.position.z =-2;
oth.position.y = 7;
oth.rotation.x = -Math.PI/2;

//Cabeça--
cabeca.position.y = 3;
//-----
//Corpo-----
corpo.position.y= 2.2112;
corpo.position.z= 0.005;
//Braço 1---
br1.position.x = -0.645;
br1.position.y = 2.9;
br1.position.z = -0.3;
//Braço 2--
br2.position.x = 0.656;
br2.position.y = 2.012;
br2.position.z = 0.090;
//Perna 1--
pr1.position.x =-0.376;
pr1.position.y =1.068;
pr1.position.z = 0.000;
//perna 2--
pr2.position.x =0.376;
pr2.position.y =1.068;
pr2.position.z = 0.000;

//Scale e rotação
br1.rotateX(90);
personagem.scale.x =2;
personagem.scale.y =2;
personagem.scale.z =2;



//Adicionar ao grupo--------------------------------
personagem.add(cabeca);
personagem.add(corpo);
personagem.add(br1);
personagem.add(br2);
personagem.add(pr1);
personagem.add(pr2);
personagem.add(oth);
personagem.add(oth2);
personagem.position.y = -8

//Fim do objeto------------------------------------


//Luz---------------
//Luz ambiente iniciada a branco e adicionada a cena
const light = new THREE.AmbientLight('#ffffff');
cena.add(light);

function Start()
{
    //Iniciar o plano e o personagem
    cena.add(personagem);

    

    requestAnimationFrame(update);
}

//Variaveis utilizadas--------

//Determina de o player perdeu ou ganhou
var hp = 0;

//Rotação do boneco
var bonecoRodar = 0;

//Variavel que controla o caminhar do boneco
var bonecoAndar = 0;

//Variavel para o movimento em primeira pessoa com o rato
var cuboCoordRotation = {x: 0, y:0};

//Modo de camara, 1 pessoa, ortogonal ou 3 pessoa
var modoCamara = 2 ;

//Modo de luz dia/noite da luz de ambiente
var tipoLuz = 0;

//Quantos inimigos o jogador matou
var score = 0;

//Velocidade do inimigo
var velocidadeImportado = 0.1;

//Controlo dos candeeiros
var tipoCandeiro = 0;

function update()
{
    //Primeira pessoa
    if(modoCamara == 1) {

        //Mexer a camara com o rato
        if(cuboCoordRotation != null)
        {          
            camara.rotation.y += cuboCoordRotation.x * -0.05;
            personagem.rotateY(cuboCoordRotation.x * -0.05);
        }

        //Fazer o boneco andar para a sua frente
        if(bonecoAndar != 0) {
            personagem.translateZ(bonecoAndar);
        }

        //Dar o movimento a bola consoante o movimento do jogador
        bola.rotateY(cuboCoordRotation.x * -0.05);
        bola.translateZ(-1.2);

        //Posicionamento da camara
        camara.position.z = personagem.position.z;
        camara.position.x = personagem.position.x; 

        //Reset das variavies utilizadas
        bonecoRodar = 0;
        bonecoAndar = 0;  
        modoCamara = 1;

        renderer.render(cena, camara);
    }
    //Camara ortografica
    if(modoCamara == 2) {

        //Movimento do jogador com as teclas AD
        if(bonecoRodar != null) {
            personagem.rotateY(bonecoRodar)
        }
        //Movimnto do jogador com as telcas WS
        if(bonecoAndar != 0) {
            personagem.translateZ(bonecoAndar);
        }

        //Rotação da bola consoante a rotação do jogaodr
        bola.rotateY(bonecoRodar);
        bola.translateZ(-1);

        //Reset das variaveis utilizadas
        bonecoRodar = 0;
        bonecoAndar = 0;
        modoCamara = 2;

        renderer.render(cena, camara2);

    }

    //Camara 3 pessoa
    if(modoCamara == 3) {

        //Movimento do boneco com as teclas WS
        if(bonecoAndar != 0) {
            personagem.translateZ(bonecoAndar);
        }

        //Rotação do boneco com as teclas AD
        if(bonecoRodar != null) {
            personagem.rotateY(bonecoRodar)
        }

        //Rotação da bola consoante a rotação do boneco
        bola.rotateY(bonecoRodar);
        bola.translateZ(-1);

        //Reset das variaveis utilizadas
        bonecoAndar = 0;
        bonecoRodar = 0;

        //Posicionamento da camara em 3 pessoa
        camara3.position.z = personagem.position.z +10;
        camara3.position.x = personagem.position.x;
        camara3.position.y = 1;
        renderer.render(cena, camara3);
        modoCamara = 3;
    }

    //Prender personagem dentro da cena
    if(personagem.position.x >74) {
        personagem.position.x = 74;
    }
    if(personagem.position.x <-74) {
        personagem.position.x = -74;
    }

    if(personagem.position.z >74) {
        personagem.position.z = 74;
    }
    if(personagem.position.z <-74) {
        personagem.position.z = -74;
    }

    //Objeto importado
    if(mixerAnimacao){
       mixerAnimacao.update(relogio.getDelta());
    }
    if(objetoImportado != null) {
        
        //Obrigar o objeto a ir para a posição do jogador
        objetoImportado.lookAt(personagem.position)
        objetoImportado.translateZ(velocidadeImportado);
        objetoImportado.position.y = -6;
        objetoImportado.rotation.x = 0
        objetoImportado.rotation.z = 0

        //Verificação se a bola se encontra na mesma posição do inimigo
        if(bola.position.x < objetoImportado.position.x + 2 && bola.position.x > objetoImportado.position.x - 2) {
            if(bola.position.z < objetoImportado.position.z + 2 && bola.position.z > objetoImportado.position.z - 2) {
                cena.remove(objetoImportado);
                score++;


                objetoImportado.position.x= THREE.Math.randInt(-50,50);
                objetoImportado.position.z= THREE.Math.randInt(-50,50);
                cena.add(objetoImportado);
                cena.remove(bola);
            }
        }
        //Verificação se o inimigo se encontra na mesma posição do jogador
        if(objetoImportado.position.x < personagem.position.x + 1 && objetoImportado.position.x > personagem.position.x - 1) {
            if(objetoImportado.position.z < personagem.position.z +1 && objetoImportado.position.z > personagem.position.z - 1) {

                //EM caso de derrota:
                cena.remove(objetoImportado);
                cena.remove(personagem);
                cena.remove(wall_1);
                cena.remove(wall_2);
                cena.remove(wall_3);
                cena.remove(wall_4);
                cena.remove(sky);
                cena.remove(floor);
                cena.remove(bola);
                cena.remove(lamp_1);
                cena.remove(lamp_2);
                cena.remove(lamp_3);
                cena.remove(lamp_4);

                //Texto final
                if(cena.getObjectByName(personagem) == null && hp == 0) {
                    var Finaldojogo = document.createElement('div');
                    Finaldojogo.style.position = 'absolute';
                    Finaldojogo.style.color = "orange";
                    Finaldojogo.style.width = 7000;
                    Finaldojogo.style.height = 7000;
                    Finaldojogo.innerHTML = "PERDEU";
                    Finaldojogo.style.top = window.innerHeight - 500 + 'px';
                    Finaldojogo.style.left =   900 + 'px';
                    document.body.appendChild(Finaldojogo);

                    var Finaldojogo = document.createElement('div');
                    Finaldojogo.style.position = 'absolute';
                    Finaldojogo.style.color = "orange";
                    Finaldojogo.style.width = 7000;
                    Finaldojogo.style.height = 7000;
                    Finaldojogo.innerHTML = "Pontuação:" + score;
                    Finaldojogo.style.top = window.innerHeight - 480 + 'px';
                    Finaldojogo.style.left =  890 + 'px';
                    document.body.appendChild(Finaldojogo);

                    hp = 1;

                    var Finaldojogo = document.createElement('div');
                    Finaldojogo.style.position = 'absolute';
                    Finaldojogo.style.color = "orange";
                    Finaldojogo.style.width = 7000;
                    Finaldojogo.style.height = 7000;
                    Finaldojogo.innerHTML = "Clique F5 para recomeçar";
                    Finaldojogo.style.top = window.innerHeight - 460 + 'px';
                    Finaldojogo.style.left = 845 + 'px';
                    document.body.appendChild(Finaldojogo);


                }
            }
        }
    }

    
    
    //LUZ ambinete
    if(tipoLuz == 0) {
        light.intensity =1;
    }
    if(tipoLuz == 1) {
        light.intensity =0.2
    }
    if(tipoCandeiro == 0) {
        luz_1.intensity = 0;
        luz_2.intensity = 0;
        luz_3.intensity = 0;
        luz_4.intensity = 0;
    }
    if(tipoCandeiro == 1) {
        luz_1.intensity = 0.7;
        luz_2.intensity = 0.7;
        luz_3.intensity = 0.7;
        luz_4.intensity = 0.7;
    }


    requestAnimationFrame(update);
}

//Rodar o boneco
document.addEventListener('keydown', ev =>{

    var rodar = 0;

    if(ev.keyCode == 65)
        rodar = 0.3;
    if(ev.keyCode == 68)
        rodar = -0.3;         
    bonecoRodar = rodar;
});

//Movimentar o boneco
document.addEventListener('keydown', ev =>{

    var andar = 0;
    if(ev.keyCode == 87)
        andar = -0.8;
    if(ev.keyCode == 83)
        andar = 0.8;     
    bonecoAndar = andar;  
});

document.addEventListener('keydown', ev =>{

    //Adicionar a bola a cena    
    if(ev.keyCode == 32) {
        cena.add(bola);
        bola.position.x = personagem.position.x;
        bola.position.y = personagem.position.y+7;
        bola.position.z = personagem.position.z-1;     
    }

    //Alternar entre dia e noite
    if(ev.keyCode == 70) {
        if(tipoLuz == 0){
            tipoLuz =1
        } else {
            tipoLuz =0;
        }
    }
    if(ev.keyCode == 67) {
        if(tipoCandeiro == 0) {
            tipoCandeiro =1;
        } else {
            tipoCandeiro = 0;
        }
            
           
    }
});

//Movimento da camara em primeira pessoa
document.addEventListener('mousemove', ev =>{
    var x = (ev.clientX - 0)/ (window.innerWidth - 0) * (1-(-1)) + -1;
    var y = (ev.clientY - 0)/ (window.innerHeight - 0) * (1-(-1)) + -1;

    cuboCoordRotation = {
        x:x,
        y:y
    }
});

document.addEventListener('keydown', ev =>{
  
    //Trocas de camara
    if(ev.keyCode == 49) {
        modoCamara = 1;
        bola.rotation.y = Math.PI / 180 * 90;
        camara.rotation.y = Math.PI / 180 * 90;
        personagem.rotation.y = Math.PI / 180 * 90;
    }
    if(ev.keyCode == 50) {
        modoCamara = 2;
        bola.rotation.y = Math.PI / 180 * 90;    
        camara.rotation.y = Math.PI / 180 * 90;
        personagem.rotation.y = Math.PI / 180 * 90;  
    }
    if(ev.keyCode == 51) {
        modoCamara = 3;   
        bola.rotation.y = Math.PI / 180 * 90;
        camara.rotation.y = Math.PI / 180 * 90;
        personagem.rotation.y = Math.PI / 180 * 90;   
    }

    //Reset com enter
    if(ev.keyCode == 13) {
        cena.add(objetoImportado);
        cena.add(personagem);
        cena.add(wall_1);
        cena.add(wall_2);
        cena.add(wall_3);
        cena.add(wall_4);
        cena.add(sky);
        cena.add(floor);
        score = 0;
        hp = 0;

        objetoImportado.position.x= THREE.Math.randInt(-50,50);
        objetoImportado.position.z= THREE.Math.randInt(-50,50);

        
        personagem.position.x= THREE.Math.randInt(-50,50);
        personagem.position.z= THREE.Math.randInt(-50,50);
        
    }
    //Pausar o inimigo
    if(ev.keyCode == 80) {
        if(velocidadeImportado == 0) {
            velocidadeImportado = 0.2;
        } else {
            velocidadeImportado = 0;
        }
    }

    //Alterar velocidade do inimigo
    if(ev.keyCode == 107) {
        velocidadeImportado = velocidadeImportado + 0.01;

    }
    if(ev.keyCode == 109) {
        velocidadeImportado = velocidadeImportado - 0.01;
        
        if(velocidadeImportado < 0.01) {
            velocidadeImportado = 0.01;
        }
    }
});





//TEXTO DE AJUDA

var texto_controlos = document.createElement('div');
texto_controlos.style.position = 'absolute';
texto_controlos.style.color = "orange";
texto_controlos.style.width = 100;
texto_controlos.style.height = 100;
texto_controlos.innerHTML = "Câmaras:";
texto_controlos.style.top = window.innerHeight - 500 + 'px';
texto_controlos.style.left = 10 + 'px';
document.body.appendChild(texto_controlos);

var texto_camara = document.createElement('div');
texto_camara.style.position = 'absolute';
texto_camara.style.color = "orange";
texto_camara.style.width = 100;
texto_camara.style.height = 100;
texto_camara.innerHTML = "1: Primeira pessoa";
texto_camara.style.top = window.innerHeight - 480 + 'px';
texto_camara.style.left = 10 + 'px';
document.body.appendChild(texto_camara);

var texto_camara = document.createElement('div');
texto_camara.style.position = 'absolute';
texto_camara.style.color = "orange";
texto_camara.style.width = 100;
texto_camara.style.height = 100;
texto_camara.innerHTML = "2: Camara Ortogonal";
texto_camara.style.top = window.innerHeight - 460 + 'px';
texto_camara.style.left = 10 + 'px';
document.body.appendChild(texto_camara);

var texto_camara = document.createElement('div');
texto_camara.style.position = 'absolute';
texto_camara.style.color = "orange";
texto_camara.style.width = 100;
texto_camara.style.height = 100;
texto_camara.innerHTML = "3: Terceira pessoa";
texto_camara.style.top = window.innerHeight - 440 + 'px';
texto_camara.style.left = 10 + 'px';
document.body.appendChild(texto_camara);

var texto_disparar = document.createElement('div');
texto_disparar.style.position = 'absolute';
texto_disparar.style.color = "orange";
texto_disparar.style.width = 100;
texto_disparar.style.height = 100;
texto_disparar.innerHTML = "Movimento:";
texto_disparar.style.top = window.innerHeight - 400 + 'px';
texto_disparar.style.left = 10 + 'px';
document.body.appendChild(texto_disparar);

var texto_disparar = document.createElement('div');
texto_disparar.style.position = 'absolute';
texto_disparar.style.color = "orange";
texto_disparar.style.width = 100;
texto_disparar.style.height = 100;
texto_disparar.innerHTML = "W/S: Frente/trás";
texto_disparar.style.top = window.innerHeight - 380 + 'px';
texto_disparar.style.left = 10 + 'px';
document.body.appendChild(texto_disparar);

var texto_disparar = document.createElement('div');
texto_disparar.style.position = 'absolute';
texto_disparar.style.color = "orange";
texto_disparar.style.width = 100;
texto_disparar.style.height = 100;
texto_disparar.innerHTML = "Rato: Movimentar câmara em 1ª pessoa";
texto_disparar.style.top = window.innerHeight - 360 + 'px';
texto_disparar.style.left = 10 + 'px';
document.body.appendChild(texto_disparar);

var texto_disparar = document.createElement('div');
texto_disparar.style.position = 'absolute';
texto_disparar.style.color = "orange";
texto_disparar.style.width = 100;
texto_disparar.style.height = 100;
texto_disparar.innerHTML = "A/D: Rodar o boneco";
texto_disparar.style.top = window.innerHeight - 340 + 'px';
texto_disparar.style.left = 10 + 'px';
document.body.appendChild(texto_disparar);

var texto_disparar = document.createElement('div');
texto_disparar.style.position = 'absolute';
texto_disparar.style.color = "orange";
texto_disparar.style.width = 100;
texto_disparar.style.height = 100;
texto_disparar.innerHTML = "Ações: ";
texto_disparar.style.top = window.innerHeight - 300 + 'px';
texto_disparar.style.left = 10 + 'px';
document.body.appendChild(texto_disparar);

var texto_disparar = document.createElement('div');
texto_disparar.style.position = 'absolute';
texto_disparar.style.color = "orange";
texto_disparar.style.width = 100;
texto_disparar.style.height = 100;
texto_disparar.innerHTML = "ESPAÇO: Disparar ";
texto_disparar.style.top = window.innerHeight - 280 + 'px';
texto_disparar.style.left = 10 + 'px';
document.body.appendChild(texto_disparar);

var texto_dianoite = document.createElement('div');
texto_dianoite.style.position = 'absolute';
texto_dianoite.style.color = "orange";
texto_dianoite.style.width = 100;
texto_dianoite.style.height = 100;
texto_dianoite.innerHTML = "F: Mudar dia/noite";
texto_dianoite.style.top = window.innerHeight - 260 + 'px';
texto_dianoite.style.left = 10 + 'px';
document.body.appendChild(texto_dianoite);

var texto_disparar = document.createElement('div');
texto_disparar.style.position = 'absolute';
texto_disparar.style.color = "orange";
texto_disparar.style.width = 100;
texto_disparar.style.height = 100;
texto_disparar.innerHTML = "C: Desligar/Ligar candeiros";
texto_disparar.style.top = window.innerHeight - 240 + 'px';
texto_disparar.style.left = 10 + 'px';
document.body.appendChild(texto_disparar);

var texto_disparar = document.createElement('div');
texto_disparar.style.position = 'absolute';
texto_disparar.style.color = "orange";
texto_disparar.style.width = 100;
texto_disparar.style.height = 100;
texto_disparar.innerHTML = "P: Pausar movimento do inimigo";
texto_disparar.style.top = window.innerHeight - 220 + 'px';
texto_disparar.style.left = 10 + 'px';
document.body.appendChild(texto_disparar);

var texto_dicas2 = document.createElement('div');
texto_dicas2.style.position = 'absolute';
texto_dicas2.style.color = "orange";
texto_dicas2.style.width = 100;
texto_dicas2.style.height = 100;
texto_dicas2.innerHTML = "+/-: Aumentar/Diminuir velocidade do inimigo";
texto_dicas2.style.top = window.innerHeight - 200 + 'px';
texto_dicas2.style.left = 10 + 'px';
document.body.appendChild(texto_dicas2);


var texto_dicas2 = document.createElement('div');
texto_dicas2.style.position = 'absolute';
texto_dicas2.style.color = "orange";
texto_dicas2.style.width = 100;
texto_dicas2.style.height = 100;
texto_dicas2.innerHTML = "Prima ENTER para dar RESET";
texto_dicas2.style.top = window.innerHeight - 180 + 'px';
texto_dicas2.style.left = 10 + 'px';
document.body.appendChild(texto_dicas2);


var texto_dicas2 = document.createElement('div');
texto_dicas2.style.position = 'absolute';
texto_dicas2.style.color = "orange";
texto_dicas2.style.width = 100;
texto_dicas2.style.height = 100;
texto_dicas2.innerHTML = "DICA:";
texto_dicas2.style.top = window.innerHeight - 120 + 'px';
texto_dicas2.style.left = 10 + 'px';
document.body.appendChild(texto_dicas2);

var texto_dicas2 = document.createElement('div');
texto_dicas2.style.position = 'absolute';
texto_dicas2.style.color = "orange";
texto_dicas2.style.width = 100;
texto_dicas2.style.height = 100;
texto_dicas2.innerHTML = "O movimento da bola é controlado pela ";
texto_dicas2.style.top = window.innerHeight - 100 + 'px';
texto_dicas2.style.left = 10 + 'px';
document.body.appendChild(texto_dicas2);

var texto_dicas2 = document.createElement('div');
texto_dicas2.style.position = 'absolute';
texto_dicas2.style.color = "orange";
texto_dicas2.style.width = 100;
texto_dicas2.style.height = 100;
texto_dicas2.innerHTML = "rotação do personagem";
texto_dicas2.style.top = window.innerHeight - 80 + 'px';
texto_dicas2.style.left = 10 + 'px';
document.body.appendChild(texto_dicas2);


