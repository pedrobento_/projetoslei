import React from 'react'
import * as GiIcons from "react-icons/gi";
import * as FaIcons from "react-icons/fa";
import * as RiIcons from "react-icons/ri";
import * as BsIcons from "react-icons/bs";
import * as VscIcons from "react-icons/vsc"

export const SidebarData = [
    {
        title: 'Alunos',
        path: '/',
        icon: <BsIcons.BsFillPersonLinesFill/>,
        cName: 'nav-text'
    },
    {
        title: 'Unidades Curriculares',
        path: '/ucs',
        icon: <RiIcons.RiBook2Fill/>,
        cName: 'nav-text'
    },
    {
        title: 'Docentes',
        path: '/docentes',
        icon: <GiIcons.GiTeacher/>,
        cName: 'nav-text'
    },
    {
        title: 'Empresas',
        path: '/empresas',
        icon: <FaIcons.FaNetworkWired/>,
        cName: 'nav-text'
    },
    {
        title: 'Tendências',
        path: '/tendencias',
        icon: <VscIcons.VscGraphLine/>,
        cName: 'nav-text'
    }
]