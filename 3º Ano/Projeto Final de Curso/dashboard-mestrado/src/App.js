import React from 'react';
import './App.css';
import Navbar from './components/Navbar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'; 

import Alunos from './pages/Alunos';
import Ucs from './pages/Ucs';
import Empresas from './pages/Empresas';
import Docentes from './pages/Docentes';
import Tendencias from './pages/Tendencias';
import Verificacao from './pages/Verificacao';



function App() {
  return (
   <>
    <Router>
    
    <div>  
         {       window.location.pathname!=='/verificacao' ? <Navbar/>:null      }   
    </div>

      <Switch>
        <Route exact path='/' component={Alunos} />
        <Route path='/ucs' component={Ucs} />
        <Route path='/empresas' component={Empresas} />
        <Route path='/docentes' component={Docentes} />
        <Route path='/tendencias' component={Tendencias} />
        <Route path='/verificacao' component={Verificacao} />
      </Switch>
    </Router>
   </>  
  );
}
export default App;


