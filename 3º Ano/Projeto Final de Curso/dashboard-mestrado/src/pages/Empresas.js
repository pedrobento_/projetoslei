import React, {Component, useEffect} from 'react';
import axios from 'axios';
import {useState} from 'react';
import './styles.css'
import { DataGrid } from '@mui/x-data-grid';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';

function Empresas() {


//GET empresas---------------------------------------------------
const[empresas, setEmpresas] = useState([]);
useEffect(() => {
  axios.get('https://localhost:5001/api/Empresas')
  .then((response) => {
    setEmpresas(response.data);
    console.log(response.data);
  });
}, []);
//-----------------------------------------------------------------
//Construção de tabela e filtro
const detailRows = empresas.map((row) => {
  return {
    id: row.id,
    nome: row.nome,
    email: row.email,
    contacto: row.telefone,
    nProjetos: row.projetoFinals.length,
  }
});

const afterFilter = detailRows.filter(empresa => {
  return empresa.id != 0;
});

const columns = [
  { field: 'nome', headerName: 'Nome', width: 200 },
  { field: 'email', headerName: 'E-mail', width: 425 },
  { field: 'contacto', headerName: 'Contacto', width: 150 },
  { field: 'nProjetos', headerName: 'Número de alunos', width: 150 },
]
//-----------------------------------------------------------------
//Dialog de informação de empresas.
const [seeData, setSeeData] = useState(false);
const [empresaData, setEmpresaData] = useState(null);

const [empresaNome, setEmpresaNome] = useState("");
const [empresaEmail, setEmpresaEmail] = useState("");
const [empresaContacto, setEmpresaContacto] = useState("");

const handleSeeDataDialogOpen = () => {
  setEmpresaNome(empresaData[0].nome);
  setEmpresaEmail(empresaData[0].email);
  setEmpresaContacto(empresaData[0].contacto);
  
  setSeeData(true);
}
const handleSeeDataDialogClose = () => {
  setSeeData(false);
}
const handleSaveEditedData = () => {
  setSeeData(false);
  axios.put("https://localhost:5001/api/Empresas/" + empresaData[0].id , {
    id: empresaData[0].id,
    nome: empresaNome,
    telefone: empresaContacto,
    email: empresaEmail,
  });
  window.location.reload(false);
}
//---------------------------------------------------------------------
//Dialog para criar empresa
const[createEmpresa, setCreateEmpresa] = useState(false);

const handleCreateEmpresaOpen = () => {
  setCreateEmpresa(true)
}
const handleCreateEmpresaClose = () => {
  setCreateEmpresa(false)
}

const handleCreateEmpresaSave = () => {
  setCreateEmpresa(false)

  axios.post('https://localhost:5001/api/Empresas', {
    nome: empresaNome,
    telefone: empresaContacto,
    email: empresaEmail
  });
  window.location.reload(false);
}

const handleDeleteEmpresa = () => {
  axios.delete('https://localhost:5001/api/Empresas/' + empresaData[0].id);

  window.location.reload(false);
}


  return (
    <>
      <div style={{ height: 700, width: '75%', margin: 'auto', marginLeft:"250px", display:"flex"}}>
     
      <DataGrid 
        rows={afterFilter}
        columns={columns}
        pageSize={10}
        rowsPerPageOptions={[10]}
        disableMultipleSelection={true}
        onSelectionModelChange={(ids) => {
          const selectedIDs = new Set(ids);
          const selectedRowData = afterFilter.filter((row) =>
            selectedIDs.has(row.id)
          );
         setEmpresaData(selectedRowData);
         handleSeeDataDialogOpen();
        }}        
      />

      <Button sx={{height: "50px", margin:"10px", backgroundColor:"#f05a26"}} variant="contained" onClick={handleCreateEmpresaOpen}>
        Adicionar Empresa
      </Button>
    </div>
    {empresaData != null && 
    <Dialog open={seeData} onClose={handleSeeDataDialogClose} fullWidth={20}>
     <DialogTitle>Nesta janela pode alterar os dados de: {empresaData[0].nome}</DialogTitle>

     <TextField sx={{width: 300, marginLeft:"150px", marginBottom:"20px"}}
        id="outlined-required"
        label="Nome"
        variant="standard"
        margin="dense"
        defaultValue={empresaData[0].nome}
        onChange={(e) => {
          setEmpresaNome(e.target.value);
        }}
      />
      <TextField sx={{width: 300, marginLeft:"150px", marginBottom:"20px"}}
        id="outlined-required"
        label="E-mail"
        variant="standard"
        margin="dense"
        defaultValue={empresaData[0].email}
        onChange={(e) => {
          setEmpresaEmail(e.target.value);
        }}
      />
      <TextField sx={{width: 300, marginLeft:"150px", marginBottom:"20px"}}
        id="outlined-required"
        label="Nome"
        variant="standard"
        margin="dense"
        defaultValue={empresaData[0].contacto}
        onChange={(e) => {
          setEmpresaContacto(e.target.value);
        }}
      />
     <DialogActions>
       <Button sx={{color:"#f05a26"}} onClick={handleSaveEditedData} >Guardar</Button>

       {empresaData[0].nProjetos == "0" &&
        <Button sx={{color:"#f05a26"}} onClick={handleDeleteEmpresa}>Remover empresa</Button>
       }    

       <Button sx={{color:"#f05a26"}} onClick={handleSeeDataDialogClose}>Fechar</Button>
     </DialogActions>
    </Dialog>
    }

    <Dialog open={createEmpresa} onClose={handleCreateEmpresaClose} fullWidth={20}>
     <DialogTitle>Adicionar empresa</DialogTitle>
     <DialogContent>
          <DialogContentText>
            Nesta janela pode criar uma empresa de forma manual.
          </DialogContentText>
          <TextField
            required
            id="outlined-required"
            label="Nome"
            variant="standard"
            fullWidth    
            margin="dense"
            onChange={(e) => {
              setEmpresaNome(e.target.value);
            }}
          />
          <TextField
            required
            id="outlined-required"
            label="E-mail"
            variant="standard"
            fullWidth     
            margin="dense"
            onChange={(e) => {
              setEmpresaEmail(e.target.value)
            }}
          />
          <TextField
            required
            id="outlined-required"
            label="Contacto"
            variant="standard"
            fullWidth
            margin="dense"
            onChange={(e) => {
              setEmpresaContacto(e.target.value)
            }}
          />
          <DialogActions>
           <Button onClick={handleCreateEmpresaSave} sx={{color:"#f05a26"}}>Adicionar</Button>
           <Button onClick={handleCreateEmpresaClose} sx={{color:"#f05a26"}}>Fechar</Button>
          </DialogActions>
        </DialogContent>
    </Dialog>

    </>
  )
}

export default Empresas