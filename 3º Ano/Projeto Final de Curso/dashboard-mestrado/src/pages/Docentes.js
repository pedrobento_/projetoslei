import React, {Component, useEffect} from 'react';
import axios from 'axios';
import {useState} from 'react';
import './styles.css'
import { DataGrid } from '@mui/x-data-grid';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';

function Docentes() {

  //GET Docentes -------------------------------------
  const[docentes, setDocentes] = useState([]);
  useEffect(() => {
    axios.get('https://localhost:5001/api/Docentes')
    .then((response) => {
      setDocentes(response.data);
      console.log(response.data);
    });
  }, []); 

  //Construção da tabela e filtro
  const detailRows = docentes.map((row) => {
    return {
      id: row.id,
      nome: row.nome,
      email: row.email,
      nProjetos: row.equipaOrientacaos.length
    }
    });

    const columns = [
      { field: 'nome', headerName: 'Nome', width: 200 },
      { field: 'email', headerName: 'Contacto', width: 425 },
      { field: 'nProjetos', headerName: 'Equipas de Orientação', width: 170 },
    ];
  //Visualização de informação
  const [docenteData, setDocente] = useState(null);

  const [docenteNome, setDocenteNome] = useState("");
  const [docenteEmail, setDocenteEmail] = useState("");

  const [viewData, setViewData] = useState(false);

  const handleViewDataOpen = () => {
    setDocenteNome(docenteData[0].nome);
    setDocenteEmail(docenteData[0].email);
    setViewData(true);
  }
  const handleViewDataClose = () => {
    setViewData(false);
  }
  const handleViewDataSave = () => {
    setViewData(false);

    axios.put("https://localhost:5001/api/Docentes/" + docenteData[0].id , {
      id: docenteData[0].id,
      email: docenteEmail,
      nome: docenteNome,
    });
    window.location.reload(false);
  }
  //-----------------------------------
  //Adicionar Docente
  const [addDocente, setAddDocente] = useState(false);

  const handleAddDocenteOpen = () => {
    setAddDocente(true);
  }
  const handleAddDocenteClose = () => {
    setAddDocente(false);
  }
  const handleAddDocenteSave = () => {
    setAddDocente(false);

    axios.post('https://localhost:5001/api/Docentes', {
      nome: docenteNome,
      email: docenteEmail
    });
    
    window.location.reload(false);
  }

  return (
    <>
    <div style={{ height: 700, width: '75%', margin: 'auto', marginLeft:"250px", display:"flex"}}>
     
     <DataGrid 
       rows={detailRows}
       columns={columns}
       pageSize={10}
       rowsPerPageOptions={[10]}
       disableMultipleSelection={true}
       onSelectionModelChange={(ids) => {
         const selectedIDs = new Set(ids);
         const selectedRowData = detailRows.filter((row) =>
           selectedIDs.has(row.id),
         );
         setDocente(selectedRowData);
         handleViewDataOpen();
         
       }}        
     />

     <Button sx={{height: "50px", margin:"10px", backgroundColor:"#f05a26"}} variant="contained" onClick={handleAddDocenteOpen}>
       Adicionar Docente
     </Button>
   </div>
   <Dialog open={addDocente} onClose={handleAddDocenteClose} fullWidth={20}>
   <DialogTitle>Adicionar Docente</DialogTitle>
   <DialogContent>
        <TextField
          required
          id="outlined-required"
          label="Nome"
          variant="standard"
          fullWidth    
          margin="dense"
          onChange={(e) => {
            setDocenteNome(e.target.value);
          }}
        />
        <TextField
          id="outlined-re0.0.quired"
          label="E-mail"
          variant="standard"
          fullWidth     
          margin="dense"
          onChange={(e) => {
            setDocenteEmail(e.target.value)
          }}
        />

        <DialogActions>
         <Button sx={{color:"#f05a26"}} onClick={handleAddDocenteSave}>Adicionar</Button>
         <Button sx={{color:"#f05a26"}} onClick={handleAddDocenteClose}>Fechar</Button>
        </DialogActions>
      </DialogContent>


   </Dialog>

   {docenteData != null &&
   <Dialog open={viewData} onClose={handleViewDataClose} fullWidth={20}>
   <DialogTitle>{docenteData[0].nome}</DialogTitle>
   <DialogContent>
        <TextField
          id="outlined-required"
          label="Nome"
          variant="standard"
          fullWidth    
          margin="dense"
          defaultValue={docenteData[0].nome}
          onChange={(e) => {
            setDocenteNome(e.target.value);
          }}
        />
        <TextField
          id="outlined-re0.0.quired"
          label="E-mail"
          variant="standard"
          fullWidth     
          margin="dense"
          defaultValue={docenteData[0].email}
          onChange={(e) => {
            setDocenteEmail(e.target.value)
          }}
        />
        <DialogActions>
         <Button sx={{color:"#f05a26"}} onClick={handleViewDataSave}>Editar</Button>
         <Button sx={{color:"#f05a26"}} onClick={handleViewDataClose}>Fechar</Button>
        </DialogActions>
      </DialogContent>
  </Dialog>
   }


    </>
  )
}

export default Docentes