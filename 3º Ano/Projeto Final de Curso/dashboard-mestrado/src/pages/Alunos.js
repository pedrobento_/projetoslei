import React, {Component, useEffect} from 'react';
import axios from 'axios';
import {useState} from 'react';
import './styles.css'
import { DataGrid } from '@mui/x-data-grid';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import { Link } from 'react-router-dom';

function useForceUpdate() {
  const [value, setValue] = useState(0);
  return () => setValue(value => value +1);
}
function Alunos() {
  
  
  const[alunos, setAlunos] = useState([]);
  const[student, setStudent] = useState(null);
  const forceUpdate = useForceUpdate();
  const[empresas, setEmpresas] = useState([]);
  //Adicionar alunos com lista
  //Gerir agrupamentos dos alunos
  
  useEffect(() => {
    axios.get('https://localhost:5001/api/Alunos')
    .then((response) => {
      setAlunos(response.data);
      console.log(response.data);
    });
  }, []);

  useEffect(() => {
    axios.get('https://localhost:5001/api/Empresas')
    .then((response) => {
      setEmpresas(response.data);
      console.log(response.data);
    });
  }, []);



  //-----------------------------------------------------------------------
  const columns = [
    { field: 'numero', headerName: 'Número', width: 70 },
    { field: 'nome', headerName: 'Nome', width: 200 },
    { field: 'tipoProjeto', headerName: 'Tipo Projeto', width: 200},
    { field: 'edicao', headerName: 'Edição', width: 100},
    { field: 'dataDefesa', headerName: 'Data da Defesa', width: 150},
    { field: 'observacoes', headerName: 'Observações', width: 500},
  ];

  //Dados da lista----------------------------------------------------------
  const detailsRows = alunos.map((row) => {
    return {
      id: row.id,
      numero: row.numero,
      nome: row.nome,
      dataDefesa: row.projeto.dataDefesa,
      observacoes: row.projeto.observacoes,
      edicao: row.edicao,
      tipoProjeto: row.projeto.tipoProjeto
    }
  });
  //Dados para apresentar em baixo--------------------------------------------------
const afterGet = alunos.map((row)=> {
  return {
    id: row.id,
    numero: row.numero,
    nome: row.nome,
    dataDefesa: row.projeto.dataDefesa,
    observacoes: row.projeto.observacoes,
    edicao: row.edicao,
    tipoProjeto: row.projeto.tipoProjeto,
    dataEntrega: row.projeto.dataEntrega,
    nomeEmpresa: row.projeto.empresa.nome,
    orientadorEmpresa: row.projeto.orientadorEmpresa,
    juri: row.projeto.juri,
    projetoId: row.projeto.id,
    empresaId: row.projeto.empresa.id
  }
});



//Dialog ver alunos----------------------
const [open, setOpen] = useState(false);
const handleClickOpen = () => {
  setOpen(true);
};
const handleClose = () => {
  setOpen(false);
};

//Dialog adicionar aluno------------
const [add, setAdd] = useState(false);
const handleAddOpen = () => {
  setAdd(true);
};
const handleAddClose = () => {
  setAdd(false);
};
const handleAddPlusClose = () => {
  setAdd(false);
  axios.post('https://localhost:5001/api/Alunos', {
  numero: addNumero,
  nome: addNome,
  edicao: addEdicao,
  projetoId: 0
  })
  window.location.reload(false);
};

 //Variaveis auxiliares de adição de aluno
 const [addNome, setAddNome] = useState("");
 const [addNumero, setAddNumero] = useState("");
 const [addEdicao, setAddEdicao] = useState("");

 //Dialog para adicionar projeto
 const [addProjeto, setAddProjeto] = useState(false);
  const handleAddProjetoOpen = () => {
    setAddProjeto(true);
  };
  const handleAddProjetoClose = () => {
    setAddProjeto(false);
  };
  const handleAddPlusCloseProjeto = () => {
    setAddProjeto(false);
    axios.post('https://localhost:5001/api/ProjetoFinals?id=' + student[0].id, {
      tipoProjeto: tipoProjeto,
      empresaId: empresaProjeto,
      orientadorEmpresa: orientadorEmpresa
    })
    window.location.reload(false);
  };

  const [tipoProjeto, setTipoProjeto] = useState("");
  const handleTipoProjetoChange = (event) => {
    setTipoProjeto(event.target.value);
  }
  const [empresaProjeto, setEmpresaProjeto] = useState(0);
  const handleChangeEmpresa = (event) => {
    setEmpresaProjeto(event.target.value);
  }
  const [orientadorEmpresa, setOrientadorEmpresa] = useState("");


  //Dialog para editar aluno
  const [editData, setEditData] = useState(false);
  const handleEditDataOpen = () => {
    setEditData(true);
    setAddNome(student[0].nome);
    setAddNumero(student[0].numero);
    setAddEdicao(student[0].edicao);
  }
  const handleEditDataClose = () => {
    setEditData(false);
  }
  const handleEditData = () => {
    setEditData(false);
    axios.put('https://localhost:5001/api/Alunos/' + student[0].id, {
      numero: addNumero,
      nome: addNome,
      edicao: addEdicao,
      id: student[0].id,
      projetoId: student[0].projetoId
    })
    window.location.reload(false);
  } 

  //Dialog para editar os dados do projeto
  const [editProjeto, setEditProjeto] = useState(false);
  const handleEditProjetoOpen = () => {
    setEditProjeto(true);
    setTipoProjeto(student[0].tipoProjeto)
    setEmpresaProjeto(student[0].empresaId)
    setOrientadorEmpresa(student[0].orientadorEmpresa)
    setDataEntrega(student[0].dataEntrega);
    setDataDefesa(student[0].dataDefesa);
    setJuri(student[0].juri);
    setObservacoes(student[0].observacoes);
  }
  const handleEditProjetoClose = () => {
    setEditProjeto(false);
  }

  const handleEditProjetoSave = () => {
    setEditProjeto(false);
    axios.put("https://localhost:5001/api/ProjetoFinals/" + student[0].projetoId , {
      tipoProjeto: tipoProjeto,
      empresaId: empresaProjeto,
      orientadorEmpresa: orientadorEmpresa,
      dataDefesa: dataDefesa,
      dataEntrega: dataEntrega,
      juri: juri,
      observacoes: observacoes,
      id: student[0].projetoId
    })
    window.location.reload(false);
  }

  const[dataEntrega, setDataEntrega] = useState("");
  const[dataDefesa, setDataDefesa] = useState("");
  const[juri, setJuri] = useState("");
  const[observacoes, setObservacoes] = useState("");

  //Dialog para delete de aluno
  const[deleteAluno, setDeleteAluno] = useState(false);
  const handleDeleteAlunoOpen = () => {
    setDeleteAluno(true);
  }
  const handleDeleteAlunoClose = () => {
    setDeleteAluno(false);
  }
  const handleDeleteAlunoSave = () => {
    setDeleteAluno(false);
    axios.delete("https://localhost:5001/api/Alunos/" + student[0].id)
    axios.delete("https://localhost:5001/api/ProjetoFinals/" + student[0].projetoId)
  }

  //-----------------------------------------------------------------------
  return (
    <>
     <div style={{ height: 700, width: '75%', margin: 'auto', marginLeft:"250px", display:"flex"}}>
     
      <DataGrid 
        rows={detailsRows}
        columns={columns}
        pageSize={10}
        rowsPerPageOptions={[10]}
        disableMultipleSelection={true}
        onSelectionModelChange={(ids) => {
          const selectedIDs = new Set(ids);
          const selectedRowData = afterGet.filter((row) =>
            selectedIDs.has(row.id)
          );
         setStudent(selectedRowData);
         forceUpdate();
         handleClickOpen();
        }}
      />

      <Button sx={{height: "50px", margin:"10px", backgroundColor:"#f05a26"}} variant="contained" onClick={handleAddOpen}>
        Adicionar Aluno
      </Button>

    </div>

    <Dialog open={add} onClose={handleAddClose} fullWidth={20}>
        <DialogTitle>Adicionar aluno</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Nesta janela pode criar um aluno de forma manual.
          </DialogContentText>
          <TextField
            required
            id="outlined-required"
            label="Nome"
            variant="standard"
            fullWidth    
            margin="dense"
            onChange={(e) => {
              setAddNome(e.target.value);
            }}
          />
          <TextField
            required
            id="outlined-required"
            label="Número"
            variant="standard"
            fullWidth     
            margin="dense"
            onChange={(e) => {
              setAddNumero(e.target.value);
            }}
          />
          <TextField
            required
            id="outlined-required"
            label="Edição"
            variant="standard"
            fullWidth
            margin="dense"
            onChange={(e) => {
              setAddEdicao(e.target.value);
            }}
          />
          <DialogActions>
           <Button onClick={handleAddPlusClose} sx={{color:"#f05a26"}}>Adicionar</Button>
           <Button onClick={handleAddClose} sx={{color:"#f05a26"}}>Fechar</Button>
          </DialogActions>
        </DialogContent>
    </Dialog>



    {student != null && 
    <Dialog open={open} onClose={handleClose} fullWidth={20}>
    <DialogTitle>{student[0].nome}</DialogTitle>
    <DialogContent>
      <DialogContentText>

      <Typography gutterBottom>   
        Nome: {student[0].nome}   
      </Typography>
      <Typography gutterBottom>   
        Número: {student[0].numero}   
      </Typography>
      <Typography gutterBottom>   
        Edição: {student[0].edicao}   
      </Typography>

      {student[0].tipoProjeto != "" &&
      <Typography gutterBottom>   
        Tipo de Projeto: {student[0].tipoProjeto}   
      </Typography>
      }

      {student[0].dataEntrega != "" &&
      <Typography gutterBottom>   
        Entregou o projeto: {student[0].dataEntrega}   
      </Typography>
      }

      {student[0].dataDefesa != "" &&
      <Typography gutterBottom>   
       Data da defesa: {student[0].dataDefesa}   
      </Typography>     
      }

      {student[0].nomeEmpresa != "" &&
      <Typography gutterBottom>   
       Empresa: {student[0].nomeEmpresa}   
      </Typography> 
      }

      {student[0].orientadorEmpresa != "" &&
      <Typography gutterBottom>   
       Orientador da empresa: {student[0].orientadorEmpresa}   
      </Typography> 
      }

      {student[0].juri != "" &&
      <Typography gutterBottom>   
       Juri: {student[0].juri}   
      </Typography> 
      }
      
      {student[0].observacoes != "" &&
      <Typography gutterBottom>   
        Observações: {student[0].observacoes}   
      </Typography>  
      }
             
      </DialogContentText>
    </DialogContent>
    <DialogActions>

      {student[0].tipoProjeto == "" &&
        <Button sx={{color:"#f05a26"}} onClick={handleAddProjetoOpen}>Adicionar Projeto</Button>  
      }
      {student[0].tipoProjeto != "" &&
        <Button sx={{color:"#f05a26"}} onClick={handleEditProjetoOpen}>Editar Projeto</Button>
      }      
      <Button sx={{color:"#f05a26"}} onClick={handleEditDataOpen}>Editar dados pessoais</Button>
      <Button sx={{color:"#f05a26"}} onClick={handleDeleteAlunoOpen}>Eliminar</Button>
      <Button sx={{color:"#f05a26"}} onClick={handleClose}>Fechar</Button>
    </DialogActions>
  </Dialog>
    }
  <Dialog open={deleteAluno} onClose={handleDeleteAlunoClose} fullWidth={10}>
   <DialogTitle>Deseja mesmo remover este aluno?</DialogTitle>
    <DialogActions>
      <Button sx={{color:"#00CC00"}} onClick={handleDeleteAlunoSave}>Sim</Button>
      <Button sx={{color:"#FF0000"}} onClick={handleDeleteAlunoClose}>Não</Button>
    </DialogActions>
  </Dialog>

  <Dialog open={addProjeto} onClose={handleAddProjetoClose} fullWidth={20}>
   <DialogTitle>Adicionar Projeto</DialogTitle>

   <FormControl sx={{width: 300, marginLeft:"150px", marginBottom:"20px"}}>
        <InputLabel id="demo-simple-select-label">Tipo de Projeto</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={tipoProjeto}
          label="Tipo Projeto"
          onChange={handleTipoProjetoChange}
        >
          <MenuItem value={"Estágio"}>Estágio</MenuItem>
          <MenuItem value={"Projeto"}>Projeto</MenuItem>
          <MenuItem value={"Dissertação"}>Dissertação</MenuItem>
        </Select>
      </FormControl>

    {tipoProjeto=="Estágio" &&

      <FormControl sx={{width: 300, marginLeft:"150px", marginBottom:"20px"}}>

      <InputLabel id="demo-simple-select-label">Empresa</InputLabel>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={empresaProjeto}
        label="Empresa"
        onChange={handleChangeEmpresa}
      >
        {empresas.map((empresa) =>(
          <MenuItem value={empresa.id}>{empresa.nome}</MenuItem>
        ))}
       
      </Select>
    </FormControl>
      }
    {empresaProjeto != 0 && tipoProjeto=="Estágio" &&

        <TextField sx={{width: 300, marginLeft:"150px", marginBottom:"20px"}}
        id="outlined-required"
        label="Orientador empresa"
        variant="standard"
        fullWidth
        margin="dense"
        onChange={(e) => {
          setOrientadorEmpresa(e.target.value);
        }}
      />
    }
    <DialogActions>
     <Button sx={{color:"#f05a26"}} onClick={handleAddPlusCloseProjeto}>Adicionar</Button>
     <Button sx={{color:"#f05a26"}} onClick={handleAddProjetoClose}>Cancelar</Button>
    </DialogActions>

  </Dialog>
  {student != null &&
  <Dialog open={editData} onClose={handleEditDataClose} fullWidth={20}>
  <DialogTitle>Editar Dados</DialogTitle>
  <DialogContent>
     <DialogContentText>
       Nesta janela pode editar os dados de um aluno.
   </DialogContentText>
  </DialogContent>
  <TextField
           sx={{margin:"15px"}}
           id="outlined-required"
           label="Nome"
           variant="standard"
           margin="dense"
           defaultValue={student[0].nome}
           onChange={(e) => {
             setAddNome(e.target.value);
           }}

         />
         <TextField
           sx={{margin:"15px"}}
           id="outlined-required"
           label="Número"
           variant="standard"   
           margin="dense"
           defaultValue={student[0].numero}
           onChange={(e) => {
             setAddNumero(e.target.value);
           }}
         />
         <TextField
           sx={{margin:"15px"}}
           id="outlined-required"
           label="Edição"
           variant="standard"
           margin="dense"
           defaultValue={student[0].edicao}
           onChange={(e) => {
             setAddEdicao(e.target.value);
           }}
         />
   <DialogActions>
    <Button sx={{color:"#f05a26"}} onClick={handleEditData}>Confirmar</Button>
    <Button sx={{color:"#f05a26"}} onClick={handleEditDataClose}>Cancelar</Button>
   </DialogActions>

 </Dialog>
  }
 {student != null &&
  <Dialog open={editProjeto} onClose={handleEditProjetoClose} fullWidth={20}>
   <DialogTitle>Editar projeto</DialogTitle>
   <DialogContent>
     <DialogContentText>
       Nesta janela pode editar os dados do projeto associado ao aluno.
   </DialogContentText>
   </DialogContent>
   <FormControl sx={{width: 300, marginLeft:"150px", marginBottom:"20px"}}>
        <InputLabel id="demo-simple-select-label">Tipo de Projeto</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={tipoProjeto}
          label="Tipo Projeto"
          onChange={handleTipoProjetoChange}
        >
          <MenuItem value={"Estágio"}>Estágio</MenuItem>
          <MenuItem value={"Projeto"}>Projeto</MenuItem>
          <MenuItem value={"Dissertação"}>Dissertação</MenuItem>
        </Select>
      </FormControl>
    {tipoProjeto=="Estágio" &&

      <FormControl sx={{width: 300, marginLeft:"150px", marginBottom:"20px"}}>

      <InputLabel id="demo-simple-select-label">Empresa</InputLabel>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={empresaProjeto}
        label="Empresa"
        onChange={handleChangeEmpresa}
      >
        {empresas.map((empresa) =>(
          <MenuItem value={empresa.id}>{empresa.nome}</MenuItem>
        ))}
       
      </Select>
    </FormControl>
      }
       {empresaProjeto != 0 && tipoProjeto=="Estágio" &&

        <TextField sx={{width: 300, marginLeft:"150px", marginBottom:"20px"}}
        id="outlined-required"
        label="Orientador empresa"
        variant="standard"
        margin="dense"
        defaultValue={student[0].orientadorEmpresa}
        onChange={(e) => {
          setOrientadorEmpresa(e.target.value);
        }}
      />
    }
    <TextField sx={{width: 300, marginLeft:"150px", marginBottom:"20px"}}
        id="outlined-required"
        label="Data de Entrega"
        variant="standard"
        margin="dense"
        defaultValue={student[0].dataEntrega}
        onChange={(e) => {
          setDataEntrega(e.target.value);
        }}
     />
     <TextField sx={{width: 300, marginLeft:"150px", marginBottom:"20px"}}
        id="outlined-required"
        label="Data de Defesa"
        variant="standard"
        margin="dense"
        defaultValue={student[0].dataDefesa}
        onChange={(e) => {
          setDataDefesa(e.target.value);
        }}
     />
     <TextField sx={{width: 300, marginLeft:"150px", marginBottom:"20px"}}
        id="outlined-required"
        label="Juri"
        variant="standard"
        margin="dense"
        defaultValue={student[0].juri}
        onChange={(e) => {
          setJuri(e.target.value);
        }}
     />
     <TextField sx={{width: 300, marginLeft:"150px", marginBottom:"20px"}}
        id="outlined-required"
        label="Observações"
        variant="standard"
        margin="dense"
        defaultValue={student[0].observacoes}
        onChange={(e) => {
          setObservacoes(e.target.value);
        }}
     />
  <DialogActions>
    <Button sx={{color:"#f05a26"}} onClick={handleEditProjetoSave}>Confirmar</Button>
    <Button sx={{color:"#f05a26"}} onClick={handleEditProjetoClose}>Cancelar</Button>
   </DialogActions>

  </Dialog>
 }
  


    </>
  )
  }
export default Alunos