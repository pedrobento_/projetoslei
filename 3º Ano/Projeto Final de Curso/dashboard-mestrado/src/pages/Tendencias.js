import React, {Component, useEffect} from 'react';
import axios from 'axios';
import {useState} from 'react';
import './styles.css'
import {Chart as ChartJS} from 'chart.js/auto'
import {Bar} from "react-chartjs-2";

function Tendencias() {

    const[alunosData, setAlunosData] = useState([]);
    useEffect(() => {
        axios.get('https://localhost:5001/api/Alunos')
        .then((response) => {
          setAlunosData(response.data);
          console.log(response.data);
        });

    }, []);

    //Projetos-------------------------------------------------------------------------------
    const estagio = alunosData.filter(item => item.projeto.tipoProjeto === "Estágio");
    const countEstagio = estagio.length;

    const dissertacao = alunosData.filter(item => item.projeto.tipoProjeto === "Dissertação");
    const countDissertacao = dissertacao.length;

    const projeto = alunosData.filter(item => item.projeto.tipoProjeto === "Projeto");
    const countProjeto = projeto.length;

    const labels = [
       "Estágio", 
       "Dissertação",
       "Projeto",
    ]

    const data = {
        labels: labels,
        datasets: [{
          label: 'Tipo de Projeto',
          backgroundColor: 'rgb(240, 90, 38)',
          borderColor: 'rgb(240, 90, 38)',
          data: [countEstagio, countDissertacao, countProjeto],
        }]
    };
    //-------------------------------------------------------------

    const edicaoTypes = alunosData
    .map(dataItem => dataItem.edicao)
    .filter((edicao, index, array) => array.indexOf(edicao) === index)

    const count = edicaoTypes
    .map(edicao => ({
        type: edicao,
        count: alunosData.filter(item => item.edicao === edicao).length
    }));
    const labelEdicao = count
    .map(e => e.type)
    const _dataEdicao = count
    .map(e => e.count)

    const dataEdicao = {
        labels: labelEdicao,
        datasets: [{
            label: 'Edição',
            backgroundColor: 'rgb(240, 90, 38)',
            borderColor: 'rgb(240, 90, 38)',
            data: _dataEdicao,
          }]
    }
    //----------------------------------------------------------------------

  return (
    <>
    <div style={{width:"500px", display:"flex", margin: "20px"}}>
     <Bar data={data}/>
     <Bar data={dataEdicao}/>
    </div>
       </>
  )
}

export default Tendencias