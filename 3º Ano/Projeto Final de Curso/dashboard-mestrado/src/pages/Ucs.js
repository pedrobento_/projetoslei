import React, {Component, useEffect} from 'react';
import axios from 'axios';
import {useState} from 'react';
import './styles.css'
import { DataGrid } from '@mui/x-data-grid';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';



function Ucs() {

  //GET UCs------------------------------
  const[ucs, setUcs] = useState([]);
  useEffect(() => {
    axios.get('https://localhost:5001/api/Ucs')
    .then((response) => {
      setUcs(response.data);
      console.log(response.data);
    });
  }, []); 

  //Construção de tabela e filtro
  const detailRows = ucs.map((row) => {
  return {
    id: row.id,
    nome: row.nome,
    edicao: row.edicao,
    nIncricoes: row.listaNota.length
  }
  });
  //--------------------------------------------
  //Dialog para adicionar uc
  const[addUC, setAddUc] = useState(false);
  const handleAddUcOpen = () => {
    setAddUc(true);
  }

  const handleAddUcClose = () => {
    setAddUc(false);
  }

  const handleAddUcSave = () => {
    setAddUc(false);
    axios.post("https://localhost:5001/api/Ucs" , {
      nome: nome,
      edicao: edicao,
    });
    window.location.reload(false);

  }
  //Variaveis da mesma;
  const [nome, setNome] = useState("");
  const [edicao, setEdicao] = useState("");
  //-------------------------------------------
  //Dialog para editar UC
  const[editUC, setEditUC] = useState(false);
  const[ucData, setUcData] = useState(null);

  const handleEditUCOpen = () => {
    setNome(ucData[0].nome);
    setEdicao(ucData[0].edicao);

    
    setEditUC(true);
  }
  const handleEditUCClose = () => {
    setEditUC(false);
  }
  const handleEditUCSave = () => {
    setEditUC(false);
    axios.put("https://localhost:5001/api/Ucs/" + ucData[0].id , {
      id: ucData[0].id,
      nome: nome,
      edicao: edicao,
    });
    window.location.reload(false);
  }

const columns = [
  { field: 'nome', headerName: 'Nome', width: 200 },
  { field: 'edicao', headerName: 'Edição', width: 425 },
  { field: 'nIncricoes', headerName: 'Inscrições', width: 150 },
] 

  return (
    <>
    <div style={{ height: 700, width: '75%', margin: 'auto', marginLeft:"250px", display:"flex"}}>
     
     <DataGrid 
       rows={detailRows}
       columns={columns}
       pageSize={10}
       rowsPerPageOptions={[10]}
       disableMultipleSelection={true}
       onSelectionModelChange={(ids) => {
         const selectedIDs = new Set(ids);
         const selectedRowData = detailRows.filter((row) =>
           selectedIDs.has(row.id),
         );
         setUcData(selectedRowData);
         handleEditUCOpen();
       }}        
     />

     <Button sx={{height: "50px", margin:"10px", backgroundColor:"#f05a26"}} variant="contained" onClick={handleAddUcOpen}>
       Adicionar UC
     </Button>
   </div>

   <Dialog open={addUC} onClose={handleAddUcClose} fullWidth={20}>
     <DialogTitle>Adicionar Unidade Curricular</DialogTitle>
     <DialogContent>
          <DialogContentText>
            Nesta janela pode criar uma Unidade Curricular de forma manual.
          </DialogContentText>
          <TextField
            required
            id="outlined-required"
            label="Nome"
            variant="standard"
            fullWidth    
            margin="dense"
            onChange={(e) => {
              setNome(e.target.value);
            }}
          />
          <TextField
            required
            id="outlined-required"
            label="Edicão"
            variant="standard"
            fullWidth     
            margin="dense"
            onChange={(e) => {
              setEdicao(e.target.value)
            }}
          />
          <DialogActions>
           <Button onClick={handleAddUcSave} sx={{color:"#f05a26"}}>Adicionar</Button>
           <Button onClick={handleAddUcClose} sx={{color:"#f05a26"}}>Fechar</Button>
          </DialogActions>
        </DialogContent>
    </Dialog>

    {ucData != null &&

   <Dialog open={editUC} onClose={handleEditUCClose} fullWidth={20}>
   <DialogTitle>Editar Unidade Curricular</DialogTitle>
   <DialogContent>
        <DialogContentText>
          Nesta janela pode editar uma Unidade Curricular de forma manual.
        </DialogContentText>
        <TextField
          id="outlined-required"
          label="Nome"
          variant="standard"
          fullWidth    
          margin="dense"
          defaultValue={ucData[0].nome}
          onChange={(e) => {
            setNome(e.target.value);
          }}
        />
        <TextField
          id="outlined-required"
          label="Edicão"
          variant="standard"
          fullWidth     
          margin="dense"
          defaultValue={ucData[0].edicao}
          onChange={(e) => {
            setEdicao(e.target.value)
          }}
        />
        <DialogActions>
         <Button onClick={handleEditUCSave} sx={{color:"#f05a26"}}>Editar</Button>
         <Button onClick={handleEditUCClose} sx={{color:"#f05a26"}}>Fechar</Button>
        </DialogActions>
      </DialogContent>
  </Dialog>
    }
    </>
  )
}

export default Ucs