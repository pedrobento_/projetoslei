﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace API.Models
{
    [Table("Aluno")]
    public partial class Aluno
    {
        public Aluno()
        {
            ListaNota = new HashSet<ListaNota>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Nome { get; set; }
        public int Numero { get; set; }
        [Required]
        [StringLength(50)]
        public string Edicao { get; set; }
        public int? ProjetoId { get; set; }

        [ForeignKey(nameof(ProjetoId))]
        [InverseProperty(nameof(ProjetoFinal.Alunos))]
        public virtual ProjetoFinal Projeto { get; set; }
        [InverseProperty("Aluno")]
        public virtual ICollection<ListaNota> ListaNota { get; set; }
    }
}
