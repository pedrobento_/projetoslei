﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace API.Models
{
    [Table("Empresa")]
    public partial class Empresa
    {
        public Empresa()
        {
            ProjetoFinals = new HashSet<ProjetoFinal>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Nome { get; set; }
        [StringLength(50)]
        public string Email { get; set; }
        [StringLength(25)]
        public string Telefone { get; set; }

        [InverseProperty(nameof(ProjetoFinal.Empresa))]
        public virtual ICollection<ProjetoFinal> ProjetoFinals { get; set; }
    }
}
