﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace API.Models
{
    [Table("UC")]
    public partial class Uc
    {
        public Uc()
        {
            ListaNota = new HashSet<ListaNota>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Nome { get; set; }
        [Required]
        [StringLength(50)]
        public string Edicao { get; set; }

        [InverseProperty("Uc")]
        public virtual ICollection<ListaNota> ListaNota { get; set; }
    }
}
