﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace API.Models
{
    [Table("Docente")]
    public partial class Docente
    {
        public Docente()
        {
            EquipaOrientacaos = new HashSet<EquipaOrientacao>();
        }

        [Key]
        public int Id { get; set; }

        [StringLength(50)]
        public string Email { get; set; }
        [Required]
        [StringLength(50)]
        public string Nome { get; set; }

        [InverseProperty(nameof(EquipaOrientacao.Docente))]
        public virtual ICollection<EquipaOrientacao> EquipaOrientacaos { get; set; }
    }
}
