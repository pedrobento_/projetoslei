﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace API.Models
{
    public partial class ListaNota
    {
        [Key]
        public int AlunoId { get; set; }
        [Key]
        [Column("UCId")]
        public int Ucid { get; set; }
        public int Nota { get; set; }

        [ForeignKey(nameof(AlunoId))]
        [InverseProperty("ListaNota")]
        public virtual Aluno Aluno { get; set; }
        [ForeignKey(nameof(Ucid))]
        [InverseProperty("ListaNota")]
        public virtual Uc Uc { get; set; }
    }
}
