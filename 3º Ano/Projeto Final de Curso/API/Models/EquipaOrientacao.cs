﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace API.Models
{
    [Table("EquipaOrientacao")]
    public partial class EquipaOrientacao
    {
        [Key]
        public int ProjetoId { get; set; }
        [Key]
        public int DocenteId { get; set; }

        [ForeignKey(nameof(DocenteId))]
        [InverseProperty("EquipaOrientacaos")]
        public virtual Docente Docente { get; set; }
        [ForeignKey(nameof(ProjetoId))]
        [InverseProperty(nameof(ProjetoFinal.EquipaOrientacaos))]
        public virtual ProjetoFinal Projeto { get; set; }
    }
}
