﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace API.Models
{
    [Table("ProjetoFinal")]
    public partial class ProjetoFinal
    {
        public ProjetoFinal()
        {
            Alunos = new HashSet<Aluno>();
            EquipaOrientacaos = new HashSet<EquipaOrientacao>();
        }

        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string DataEntrega { get; set; }
        [StringLength(50)]
        public string DataDefesa { get; set; }
        [Required]
        [StringLength(50)]
        public string TipoProjeto { get; set; }
        [StringLength(50)]
        public string Juri { get; set; }
        public int? EmpresaId { get; set; }
        [StringLength(50)]
        public string OrientadorEmpresa { get; set; }
        [StringLength(200)]
        public string Observacoes { get; set; }

        [ForeignKey(nameof(EmpresaId))]
        [InverseProperty("ProjetoFinals")]
        public virtual Empresa Empresa { get; set; }
        [InverseProperty(nameof(Aluno.Projeto))]
        public virtual ICollection<Aluno> Alunos { get; set; }
        [InverseProperty(nameof(EquipaOrientacao.Projeto))]
        public virtual ICollection<EquipaOrientacao> EquipaOrientacaos { get; set; }
    }
}
