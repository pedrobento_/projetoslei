﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API.Data;
using API.Models;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UcsController : ControllerBase
    {
        private readonly APIContext _context;

        public UcsController(APIContext context)
        {
            _context = context;
        }

        // GET: api/Ucs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Uc>>> GetUcs()
        {
            return await _context.Ucs.ToListAsync();
        }

        // GET: api/Ucs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Uc>> GetUc(int id)
        {
            var uc = await _context.Ucs.FindAsync(id);

            if (uc == null)
            {
                return NotFound();
            }

            return uc;
        }

        // PUT: api/Ucs/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUc(int id, Uc uc)
        {
            if (id != uc.Id)
            {
                return BadRequest();
            }

            _context.Entry(uc).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UcExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Ucs
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Uc>> PostUc(Uc uc)
        {
            uc.Id = _context.Ucs.OrderBy(x => x.Id).Last().Id + 1;

            _context.Ucs.Add(uc);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UcExists(uc.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetUc", new { id = uc.Id }, uc);
        }

        // DELETE: api/Ucs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUc(int id)
        {
            var uc = await _context.Ucs.FindAsync(id);
            if (uc == null)
            {
                return NotFound();
            }

            _context.Ucs.Remove(uc);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool UcExists(int id)
        {
            return _context.Ucs.Any(e => e.Id == id);
        }
    }
}
