﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API.Data;
using API.Models;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjetoFinalsController : ControllerBase
    {
        private readonly APIContext _context;

        public ProjetoFinalsController(APIContext context)
        {
            _context = context;
        }

        // GET: api/ProjetoFinals
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjetoFinal>>> GetProjetoFinals()
        {
            return await _context.ProjetoFinals.ToListAsync();
        }

        // GET: api/ProjetoFinals/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProjetoFinal>> GetProjetoFinal(int id)
        {
            var projetoFinal = await _context.ProjetoFinals.FindAsync(id);

            if (projetoFinal == null)
            {
                return NotFound();
            }

            return projetoFinal;
        }

        // PUT: api/ProjetoFinals/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProjetoFinal(int id, ProjetoFinal projetoFinal)
        {
            if (id != projetoFinal.Id)
            {
                return BadRequest();
            }

            if(projetoFinal.TipoProjeto != "Estágio")
            {
                projetoFinal.OrientadorEmpresa = "";
                projetoFinal.EmpresaId = 0;
            }


            _context.Update(projetoFinal);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProjetoFinalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ProjetoFinals
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ProjetoFinal>> PostProjetoFinal(ProjetoFinal projetoFinal, int id)
        {
            projetoFinal.Id = _context.ProjetoFinals.OrderBy(x => x.Id).Last().Id + 1;
            projetoFinal.DataDefesa = "";
            projetoFinal.DataEntrega = "";
            projetoFinal.Juri = "";
            projetoFinal.Observacoes = "";

            var aluno = _context.Alunos.Find(id);
            aluno.ProjetoId = projetoFinal.Id;
            _context.Update(aluno);


            _context.ProjetoFinals.Add(projetoFinal);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProjetoFinalExists(projetoFinal.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetProjetoFinal", new { id = projetoFinal.Id }, projetoFinal);
        }

        // DELETE: api/ProjetoFinals/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProjetoFinal(int id)
        {
            var projetoFinal = await _context.ProjetoFinals.FindAsync(id);
            if (projetoFinal == null)
            {
                return NotFound();
            }

            _context.ProjetoFinals.Remove(projetoFinal);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ProjetoFinalExists(int id)
        {
            return _context.ProjetoFinals.Any(e => e.Id == id);
        }
    }
}
