﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using API.Models;

#nullable disable

namespace API.Data
{
    public partial class APIContext : DbContext
    {
        public APIContext()
        {
        }

        public APIContext(DbContextOptions<APIContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Aluno> Alunos { get; set; }
        public virtual DbSet<Docente> Docentes { get; set; }
        public virtual DbSet<Empresa> Empresas { get; set; }
        public virtual DbSet<EquipaOrientacao> EquipaOrientacaos { get; set; }
        public virtual DbSet<ListaNota> ListaNotas { get; set; }
        public virtual DbSet<ProjetoFinal> ProjetoFinals { get; set; }
        public virtual DbSet<Uc> Ucs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("name=APIContext");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Aluno>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Edicao).IsUnicode(false);

                entity.Property(e => e.Nome).IsUnicode(false);

                entity.HasOne(d => d.Projeto)
                    .WithMany(p => p.Alunos)
                    .HasForeignKey(d => d.ProjetoId)
                    .HasConstraintName("FK__Aluno__ProjetoId__2C3393D0");
            });

            modelBuilder.Entity<Docente>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.Nome).IsUnicode(false);
            });

            modelBuilder.Entity<Empresa>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.Nome).IsUnicode(false);

                entity.Property(e => e.Telefone).IsUnicode(false);
            });

            modelBuilder.Entity<EquipaOrientacao>(entity =>
            {
                entity.HasKey(e => new { e.ProjetoId, e.DocenteId })
                    .HasName("PK__EquipaOr__FE92370B36C092EA");

                entity.HasOne(d => d.Docente)
                    .WithMany(p => p.EquipaOrientacaos)
                    .HasForeignKey(d => d.DocenteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__EquipaOri__Docen__33D4B598");

                entity.HasOne(d => d.Projeto)
                    .WithMany(p => p.EquipaOrientacaos)
                    .HasForeignKey(d => d.ProjetoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__EquipaOri__Proje__32E0915F");
            });

            modelBuilder.Entity<ListaNota>(entity =>
            {
                entity.HasKey(e => new { e.AlunoId, e.Ucid })
                    .HasName("PK__ListaNot__FAF025B1A84738E6");

                entity.HasOne(d => d.Aluno)
                    .WithMany(p => p.ListaNota)
                    .HasForeignKey(d => d.AlunoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ListaNota__Aluno__2F10007B");

                entity.HasOne(d => d.Uc)
                    .WithMany(p => p.ListaNota)
                    .HasForeignKey(d => d.Ucid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ListaNotas__UCId__300424B4");
            });

            modelBuilder.Entity<ProjetoFinal>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.DataDefesa).IsUnicode(false);

                entity.Property(e => e.DataEntrega).IsUnicode(false);

                entity.Property(e => e.Juri).IsUnicode(false);

                entity.Property(e => e.Observacoes).IsUnicode(false);

                entity.Property(e => e.OrientadorEmpresa).IsUnicode(false);

                entity.Property(e => e.TipoProjeto).IsUnicode(false);

                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.ProjetoFinals)
                    .HasForeignKey(d => d.EmpresaId)
                    .HasConstraintName("FK__ProjetoFi__Empre__29572725");
            });

            modelBuilder.Entity<Uc>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nome).IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
